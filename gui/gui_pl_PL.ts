<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>DragonKingController</name>
    <message>
        <location filename="dragonkingcontroller.cpp" line="78"/>
        <source>Red Dragon</source>
        <translation>Varthrax</translation>
    </message>
    <message>
        <location filename="dragonkingcontroller.cpp" line="80"/>
        <source>Green Dragon</source>
        <translation>Grilipus</translation>
    </message>
    <message>
        <location filename="dragonkingcontroller.cpp" line="82"/>
        <source>Yellow Dragon</source>
        <translation>Cadorus</translation>
    </message>
    <message>
        <location filename="dragonkingcontroller.cpp" line="84"/>
        <source>Dragon&apos;s Attack</source>
        <translation>Smocze Natarcie</translation>
    </message>
    <message>
        <location filename="dragonkingcontroller.cpp" line="86"/>
        <source>Dragon Sleep</source>
        <translation>Smocza Drzemka</translation>
    </message>
    <message>
        <location filename="dragonkingcontroller.cpp" line="88"/>
        <source>Dragon Rage</source>
        <translation>Smoczy Gniew</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="13"/>
        <source>Dragon Counter</source>
        <translation>Licznik żetonów smoków</translation>
    </message>
    <message>
        <location filename="main.qml" line="113"/>
        <source>Draw one</source>
        <translation>Wylosuj żeton</translation>
    </message>
</context>
</TS>
