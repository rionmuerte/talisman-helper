#include "dragonkingcontroller.h"

namespace {

    QString dragon_color(Dragon token) noexcept
    {
        switch (token)
        {
        case Dragon::Red:
            return "red";
        case Dragon::Green:
            return "green";
        case Dragon::Yellow:
            return "yellow";
        }
        return "white";
    }
}

DragonKingController::DragonKingController(QObject *parent)
    : QObject(parent), tokens(0)
{
    auto king = static_cast<quint8>(counter.current_king());
    left = static_cast<Dragon>((king + 1) % 3);
    right = static_cast<Dragon>((king + 2) % 3);
}

QString DragonKingController::left_dragon()
{
    return dragon_color(left);
}

QString DragonKingController::right_dragon()
{
    return dragon_color(right);
}

QString DragonKingController::current_king()
{
    return dragon_color(counter.current_king());
}

QString DragonKingController::right_counter()
{
    return QString::number(counter.state(right));
}

QString DragonKingController::left_counter()
{
    return QString::number(counter.state(left));
}

QString DragonKingController::king_counter()
{
    const auto king = counter.current_king();
    return QString::number(counter.state(king));
}

void DragonKingController::draw()
{
    const auto king = counter.current_king();
    const auto token = bag.take_one();
    counter.add_token(token);
    if (auto new_king = counter.current_king(); new_king != king)
    {
        if (new_king == left) left = king;
        if (new_king == right) right = king;
        emit king_changed();
    }
    emit counter_changed();
    emit new_token(QString::number(++tokens),token_name(token));
}

QString DragonKingController::token_name(DragonToken token)
{
    switch (token) {
    case DragonToken::Red:
        return tr("Red Dragon");
    case DragonToken::Green:
        return tr("Green Dragon");
    case DragonToken::Yellow:
        return tr("Yellow Dragon");
    case DragonToken::Double:
        return tr("Dragon's Attack");
    case DragonToken::Sleep:
        return tr("Dragon Sleep");
    case DragonToken::Rage:
        return tr("Dragon Rage");
    }
}
