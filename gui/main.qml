import QtQuick 2.15
import QtQuick.Window 2.15
import qmlcomponents 1.0
import QtQuick.Controls 2.15

Window {
    id: window
    width: 1280
    height: 720
    minimumWidth: 640
    minimumHeight: 480
    visible: true
    title: qsTr("Dragon Counter")

    DragonKingController {
        id: controller
    }

    Shortcut {
        sequences: [
            Qt.Key_Enter,
            "Space"
        ]
        onActivated: controller.draw()
    }

    Rectangle{
        id: top_dragon
        height: window.height * 2 / 3
        color: controller.king
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top

        Text {
            id: top_label
            text: controller.king_counter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 180 * window.height / 720
            anchors.horizontalCenter: parent.horizontalCenter
        }

        ListView {
            id: listView
            width: 300 * window.width / 1280
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            model: ListModel { id: model }
            delegate: Token { }


            Connections {
                target: controller
                function onNew_token(index, token) {
                    model.insert(0,{index: index, value: token})
                }
            }

        }
    }

    Rectangle {
        id: left_dragon
        color: controller.left_dragon
        anchors.left: parent.left
        anchors.right: parent.horizontalCenter
        anchors.top: top_dragon.bottom
        anchors.bottom: parent.bottom

        Text {
            id: left_label
            text: controller.left_counter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 120 * window.height / 720
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }

    Rectangle {
        id: right_dragon
        color: controller.right_dragon
        anchors.left: parent.horizontalCenter
        anchors.right: parent.right
        anchors.top: top_dragon.bottom
        anchors.bottom: parent.bottom

        Text {
            id: right_label
            text: controller.right_counter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 120 * window.height / 720
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }

    RoundButton {
        id: roundButton
        x: window.width / 2 - width / 2
        height: 50
        width: 250
        text: qsTr("Draw one")
        anchors.bottom: parent.bottom

        Connections {
            target: roundButton
            function onClicked() {
                controller.draw()
            }
        }
    }

}
