QT += quick

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        dragonkingcontroller.cpp \
        main.cpp

RESOURCES += qml.qrc

TRANSLATIONS += \
    gui_en_US.ts \
    gui_pl_PL.ts
CONFIG += lrelease
CONFIG += embed_translations
CONFIG += qmltypes
QML_IMPORT_NAME = qmlcomponents
QML_IMPORT_MAJOR_VERSION = 1

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    dragonkingcontroller.h

unix: LIBS += -L$$OUT_PWD/../dragon-counter/ -ldragon-counter

INCLUDEPATH += $$PWD/../dragon-counter
DEPENDPATH += $$PWD/../dragon-counter

unix: PRE_TARGETDEPS += $$OUT_PWD/../dragon-counter/libdragon-counter.a

DISTFILES += \
    Token.qml \
    gui_pl_PL.ts
