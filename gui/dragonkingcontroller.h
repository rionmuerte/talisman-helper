#ifndef DRAGONKINGCONTROLLER_H
#define DRAGONKINGCONTROLLER_H

#include <QObject>
#include <qqml.h>
#include "tokenbag.h"
#include "kingcounter.h"

class DragonKingController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString king READ current_king NOTIFY king_changed)
    Q_PROPERTY(QString king_counter READ king_counter NOTIFY counter_changed)
    Q_PROPERTY(QString left_counter READ left_counter NOTIFY counter_changed)
    Q_PROPERTY(QString right_counter READ right_counter NOTIFY counter_changed)
    Q_PROPERTY(QString left_dragon READ left_dragon NOTIFY king_changed)
    Q_PROPERTY(QString right_dragon READ right_dragon NOTIFY king_changed)
    QML_ELEMENT
public:
    explicit DragonKingController(QObject *parent = nullptr);
    QString left_dragon();
    QString right_dragon();
    QString current_king();
    QString right_counter();
    QString left_counter();
    QString king_counter();
public slots:
    void draw();
signals:
    void king_changed();
    void counter_changed();
    void new_token(QString, QString);
private:
    QString token_name(DragonToken);

    Dragon left;
    Dragon right;
    quint32 tokens;
    KingCounter counter;
    TokenBag bag;
};

#endif // DRAGONKINGCONTROLLER_H
