import QtQuick 2.15

Item {
    id: token_delegate
    width: row.width + 25
    height: 50
    Row {
        id: row
        spacing: 10
        Text {
            text: index
            font.pointSize: 28
        }
        Text {
            text: value
            font.pointSize: 28
        }
    }
}
