#include <QCoreApplication>
#include <QLocale>
#include <QTranslator>
#include "tokenbag.h"
#include "kingcounter.h"


void loop();
QString dragon_token(DragonToken);
QString dragon(Dragon);

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);
    QTranslator translator;
    translator.load(QLocale(), QLatin1String("cli"), QLatin1String("_"), QLatin1String(":/i18n"));
    QCoreApplication::installTranslator(&translator);
    loop();
    return 0;
}

QString dragon_token(DragonToken token)
{
    switch (token)
    {
    case DragonToken::Red:
        return QCoreApplication::translate("main", "Red Dragon");
    case DragonToken::Green:
        return QCoreApplication::translate("main", "Green Dragon");
    case DragonToken::Yellow:
        return QCoreApplication::translate("main", "Yellow Dragon");
    case DragonToken::Sleep:
        return QCoreApplication::translate("main", "Dragon Sleep Token");
    case DragonToken::Double:
        return QCoreApplication::translate("main", "Dragon's Attack Token");
    case DragonToken::Rage:
        return QCoreApplication::translate("main", "Dragon Rage Token");
    }
    return "";
}

QString dragon(Dragon dragon)
{
    switch (dragon)
    {
    case Dragon::Red:
        return QCoreApplication::translate("main", "Red Dragon");
    case Dragon::Green:
        return QCoreApplication::translate("main", "Green Dragon");
    case Dragon::Yellow:
        return QCoreApplication::translate("main", "Yellow Dragon");
    }

    return "";
}

void loop()
{
    QTextStream out(stdout);
    TokenBag bag;
    KingCounter counter;
    out << QCoreApplication::translate("main", "Press enter to to draw one token") << Qt::endl
        << QCoreApplication::translate("main", "Current King:") << " "
        << dragon(counter.current_king()) << Qt::endl;
    while (true)
    {
        getchar();
        const auto token = bag.take_one();
        counter.add_token(token);
        out << QCoreApplication::translate("main", "Token") << " "
                    << dragon_token(token) << Qt::endl
                    << QCoreApplication::translate("main", "Current King:") << " "
                    << dragon(counter.current_king()) << Qt::endl;
        const auto state = counter.current_state();
        auto it = state.constBegin();
        while (it != state.constEnd())
        {
            out << dragon(it.key()) << " " << it.value() << Qt::endl;
            ++it;
        }
    }
}
