QT -= gui

CONFIG += c++20 console
CONFIG -= app_bundle

SOURCES += \
        main.cpp

TRANSLATIONS += \
    cli_en_US.ts \
    cli_pl_PL.ts

CONFIG += lrelease
CONFIG += embed_translations

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS +=

unix: LIBS += -L$$OUT_PWD/../dragon-counter/ -ldragon-counter

INCLUDEPATH += $$PWD/../dragon-counter
DEPENDPATH += $$PWD/../dragon-counter

unix: PRE_TARGETDEPS += $$OUT_PWD/../dragon-counter/libdragon-counter.a
