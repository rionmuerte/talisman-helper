<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>main</name>
    <message>
        <source>Red Dragon</source>
        <translation>Varthrax</translation>
    </message>
    <message>
        <source>Green Dragon</source>
        <translation>Grilipus</translation>
    </message>
    <message>
        <source>Yellow Dragon</source>
        <translation>Cadorus</translation>
    </message>
    <message>
        <source>Dragon Sleep Token</source>
        <translation>Dragon Sleep</translation>
    </message>
    <message>
        <source>Dragon&apos;s Attack Token</source>
        <translation>Dragon&apos;s Attack</translation>
    </message>
    <message>
        <source>Dragon Rage Token</source>
        <translation>Dragon Rage</translation>
    </message>
    <message>
        <source>Press enter to to draw one token</source>
        <translation>Press enter to draw one token</translation>
    </message>
    <message>
        <source>Current King:</source>
        <translation>Current king:</translation>
    </message>
    <message>
        <source>Token</source>
        <translation>Token</translation>
    </message>
</context>
</TS>
