<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>main</name>
    <message>
        <source>Red Dragon</source>
        <translation>Varthrax</translation>
    </message>
    <message>
        <source>Green Dragon</source>
        <translation>Grilipus</translation>
    </message>
    <message>
        <source>Yellow Dragon</source>
        <translation>Cadorus</translation>
    </message>
    <message>
        <source>Dragon Sleep Token</source>
        <translation>Smocza Drzemka</translation>
    </message>
    <message>
        <source>Dragon&apos;s Attack Token</source>
        <translation>Smocze Natarcie</translation>
    </message>
    <message>
        <source>Dragon Rage Token</source>
        <translation>Smoczy Gniew</translation>
    </message>
    <message>
        <source>Press enter to to draw one token</source>
        <translation>Naciśnij enter, żeby wylosować żeton</translation>
    </message>
    <message>
        <source>Current King:</source>
        <translation>Aktualny król:</translation>
    </message>
    <message>
        <source>Token</source>
        <translation>Żeton</translation>
    </message>
</context>
</TS>
