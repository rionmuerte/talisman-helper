#ifndef DRAGONS_H
#define DRAGONS_H

#include <QtCore>

enum class Dragon : quint8
{
    Red = 0,
    Green,
    Yellow,
};

enum class DragonToken : quint8
{
    Red = 0,
    Green,
    Yellow,
    Double,
    Rage,
    Sleep,
};

#endif // DRAGONS_H
