#ifndef KINGCOUNTER_H
#define KINGCOUNTER_H

#include <QMap>
#include "dragons.h"

class KingCounter
{
public:
    KingCounter();
    void add_token(DragonToken);
    Dragon current_king();
    QMap<Dragon, quint8> current_state();
    quint8 state(Dragon);
private:
    void update_king();

    Dragon king;
    QMap<Dragon, quint8> dragon_counter;
};

#endif // KINGCOUNTER_H
