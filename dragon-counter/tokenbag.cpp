#include "tokenbag.h"

TokenBag::TokenBag() : rng(QRandomGenerator::securelySeeded())
{
    reset_bag();
}

DragonToken TokenBag::take_one()
{
    if (token_bag.size() == 0)
        reset_bag();
    return token_bag.takeAt(rng.bounded(token_bag.size()));
}

void TokenBag::reset_bag()
{
    if (token_bag.size() > 0)
        token_bag.clear();
    token_bag.insert(0, 40, DragonToken::Green);
    token_bag.insert(0, 40, DragonToken::Red);
    token_bag.insert(0, 40, DragonToken::Yellow);
    token_bag.insert(0, 6, DragonToken::Double);
    token_bag.insert(0, 6, DragonToken::Rage);
    token_bag.insert(0, 8, DragonToken::Sleep);
}
