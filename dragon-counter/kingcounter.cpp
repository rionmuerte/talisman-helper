#include "kingcounter.h"

#include <array>
#include <algorithm>
#include <QRandomGenerator>

namespace {

    constexpr std::array dragons{Dragon::Red, Dragon::Green, Dragon::Yellow};

}

KingCounter::KingCounter() :
    king(dragons[QRandomGenerator::global()->bounded(static_cast<quint32>(dragons.size()))]),
      dragon_counter({{Dragon::Red, 0},
                      {Dragon::Green, 0},
                      {Dragon::Yellow, 0}})
{}

void KingCounter::add_token(DragonToken token)
{
    Dragon dragon;
    switch (token)
    {
    case DragonToken::Red:
        dragon = Dragon::Red; break;
    case DragonToken::Green:
        dragon = Dragon::Green; break;
    case DragonToken::Yellow:
        dragon = Dragon::Yellow; break;
    default:
        return;
    }
    const auto counter = dragon_counter[dragon];
    dragon_counter[dragon] = counter + 1;
    update_king();
}

Dragon KingCounter::current_king()
{
    return king;
}

QMap<Dragon, quint8> KingCounter::current_state()
{
    return dragon_counter;
}

quint8 KingCounter::state(Dragon dragon)
{
    return dragon_counter[dragon];
}

void KingCounter::update_king()
{
    for (const auto dragon: dragons)
    {
        if (dragon_counter[dragon] >= 3)
        {
            king = dragon;
            dragon_counter[dragon] = 0;
        }
    }
}
