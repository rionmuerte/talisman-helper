#ifndef TOKENBAG_H
#define TOKENBAG_H

#include <QRandomGenerator>
#include <QVector>
#include "dragons.h"

class TokenBag
{
public:
    TokenBag();
    DragonToken take_one();
private:
    void reset_bag();

    QRandomGenerator rng;
    QVector<DragonToken> token_bag;
};

#endif // TOKENBAG_H
